# Submit your Timesheets

## Get started

This project is built with Node.JS and CommonJS modules. That means you cannot `import` but have to `require` packages.

Speaking of packages, this package uses:

- NodeMailer to send emails
- node-fetch to contact external APIs
- dotenv to load environment variables

## Objective

The goal of this project is to load a list of employees who did not submit their timesheet before date
and send them an email to remind them to do so. It also includes a meme in the email to make it look more friendly.

## Knowledge

In order to contribute to this project, you need to understand the following points:

- differences between browser JavaScript and Node.JS
- differences between CJS and ECM modules
- the async/await pattern
- how to setup crontab jobs on Linux system

## CI/CD pipeline and required files

### Pipelines

The CI/CD pipelines are exposed through the [gitlab-ci.yml](./gitlab-ci.yml) file. It includes 2 paths :
1. the first pipeline is [build_and_test.yml](./pipelines/build_and_test.yml). It is triggered in two situations :
    - when a merge request is created or updated ;
    - when a new commit is added to the trunk (`master` branch).
2. the second pipeline is [deploy.yml](./pipelines/deploy.yml). It is only triggered when a new commit is added to the trunk.
Actually, this second pipeline forces the execution of the first one by including it directly in its stages.

### On-server required files

For the system to be daily triggered, a shell script is to be called by a scheduled job (cron) every day. Currently, the cron is executed every day at 9 am.

The script contains the following command :
```shell
/root/.nvm/versions/node/v16.15.0/bin/node /var/nodejs/submit-your-timesheets/build/index.js >> /var/nodejs/submit-your-timesheets/entry.log 2>&1
```
It basically means "execute with node the index.js file and print everything from stdout and stderror to the entry.log file". Simple as that!


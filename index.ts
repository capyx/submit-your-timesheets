import * as dotenv from 'dotenv';
dotenv.config({ path: __dirname + '/.env' });

import { load as loadConfiguration } from './src/config';
import TimesheetReminder from './src/TimesheetReminder';
import { TimesheetTime } from './src/Models';
import { TimeManager, ImageService, EmailManager } from './src/Services';

try {
    console.log(`[${new Date()}] Launch of the day!`);

    const config = loadConfiguration();

    const timeManager = new TimeManager(
        config.submissionConfiguration.dayOfMonthForProvisionalTimesheetSubmission,
        config.submissionConfiguration.dayOfMonthForFinalTimesheetSubmission
    );
    const memeGenerator = new ImageService(config.imageServiceOptions.apiKey, config.imageServiceOptions.apiUrl);
    const emailManager = new EmailManager(config.authenticationServiceOptions.apiKey, config.authenticationServiceOptions.apiUrl, {
        user: config.mailOptions.email,
        pass: config.mailOptions.password
    });
    const emailReminderManager = new TimesheetReminder(timeManager, memeGenerator, emailManager, config.isDevEnvironment);
    emailReminderManager.remind({
        fromName: config.mailOptions.accountName,
        fromEmail: config.mailOptions.email,
        toEmails: config.mailOptions.directRecipients,
        formatSubject: (timesheetTime: TimesheetTime) => `Timesheet ${timesheetTime.monthAsString} ${timesheetTime.year}`,
        imageQuery: config.imageServiceOptions.searchQuery,
        message: config.mailOptions.reminderConfiguration.message,
        lastMessage: config.mailOptions.reminderConfiguration.lastMessage,
        priority: config.mailOptions.reminderConfiguration.priority
    }).then(() => console.log('Reminder... Done!'));
}
catch (err: unknown) {
    const error = err as Error;
    if (error.message.startsWith('Check that you have those environments variables set')) {
        console.error('Missing environment variables. Original Error: ', error);
    }
    else if (error.message.includes('Check that the date is valid for the run!')) {
        console.error('Date is not valid. Original Error: ', error);
    }
    else {
        console.error(err);
    }
}

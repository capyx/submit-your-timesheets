import { Configuration } from './Models';
import { asPriority } from './Models/Priority';

export const environmentMandatoryKeys = [
    'GOOGLE_IMAGE_API_KEY',
    'GOOGLE_IMAGE_API_URL',
    'CAUTHPYX_URL',
    'CAUTHPYX_API_KEY',
    'GMAIL_ACCOUNT_NAME',
    'GMAIL_ACCOUNT_EMAIL',
    'GMAIL_ACCOUNT_PASSWORD',
    'DAY_OF_MONTH_FOR_PROVISIONAL_TIMESHEET_SUBMISSION',
    'DAY_OF_MONTH_FOR_FINAL_TIMESHEET_SUBMISSION',
    'DIRECT_RECIPIENTS',
    'NODE_ENV'
];

export function load(): Configuration {
    const missingKeys = environmentMandatoryKeys.filter(key => !(key in process.env));
    if (missingKeys.length > 0) {
        throw new Error(`Check that you have those environments variables set: ${missingKeys.join(', ')}`);
    }

    const config: Configuration = {
        isDevEnvironment: process.env.NODE_ENV?.toLowerCase() === 'development',
        imageServiceOptions: {
            searchQuery: process.env.IMAGE_SEARCH_QUERY!,
            apiKey: process.env.GOOGLE_IMAGE_API_KEY!,
            apiUrl: process.env.GOOGLE_IMAGE_API_URL!
        },
        authenticationServiceOptions: {
            apiKey: process.env.CAUTHPYX_API_KEY!,
            apiUrl: process.env.CAUTHPYX_URL!
        },
        mailOptions: {
            accountName: process.env.GMAIL_ACCOUNT_NAME!,
            email: process.env.GMAIL_ACCOUNT_EMAIL!,
            password: process.env.GMAIL_ACCOUNT_PASSWORD!,
            reminderConfiguration: {
                priority: asPriority(process.env.REMINDER_PRIORITY),
                message: process.env.REMINDER_MESSAGE!,
                lastMessage: process.env.LAST_REMINDER_MESSAGE!
            },
            directRecipients: process.env.DIRECT_RECIPIENTS!
        },
        submissionConfiguration: {
            dayOfMonthForProvisionalTimesheetSubmission: parseInt(process.env.DAY_OF_MONTH_FOR_PROVISIONAL_TIMESHEET_SUBMISSION!),
            dayOfMonthForFinalTimesheetSubmission: parseInt(process.env.DAY_OF_MONTH_FOR_FINAL_TIMESHEET_SUBMISSION!)
        }
    };

    return config;
}

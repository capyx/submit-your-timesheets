import { Priority } from './Priority';

export type EmailData = {
    fromName: string;
    fromEmail: string;
    toEmails: string[] | string;
    subject: string;
    contentAsHtml: string;
    contentAsText: string;
    bccEmails?: string[] | string;
    priority?: Priority;
};

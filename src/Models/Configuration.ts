import { Priority } from './Priority';

type ExternalProviderOptions = {
    apiKey: string;
    apiUrl: string;
};

type MailOptions = {
    accountName: string;
    email: string;
    password: string;

    reminderConfiguration: {
        priority: Priority;
        message: string;
        lastMessage: string;
    };

    directRecipients: string | string[];
};

type SubmissionConfiguration = {
    dayOfMonthForProvisionalTimesheetSubmission: number;
    dayOfMonthForFinalTimesheetSubmission: number;
}

export type Configuration = {
    isDevEnvironment: boolean;
    imageServiceOptions: {
        searchQuery: string;
    } & ExternalProviderOptions;
    authenticationServiceOptions: ExternalProviderOptions;
    mailOptions: MailOptions;
    submissionConfiguration: SubmissionConfiguration;
};

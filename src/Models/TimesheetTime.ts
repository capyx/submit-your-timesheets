export type TimesheetTime = {
    month: number;
    monthAsString: string;
    year: number;
    isFinalTimesheetTime: boolean;
};

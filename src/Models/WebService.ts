export type WebService = {
    apiKey: string;
    baseUrl: string;
};

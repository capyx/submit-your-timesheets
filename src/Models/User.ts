export type User = {
    id: number;
    email: string;
    roleId: number;
    status: boolean;
    isActive: boolean;
    creationDate: string;
    updateDate: string;
};

const PRIORITIES = ['high', 'normal', 'low'] as const;
type PrioritiesTuple = typeof PRIORITIES;

export type Priority = PrioritiesTuple[number];

function isPriority(str: string | undefined): str is Priority {
    return PRIORITIES.includes(str as Priority);
}

export function asPriority(str: string | undefined): Priority {
    if (isPriority(str)) return str;
    return 'high';
}

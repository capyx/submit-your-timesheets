import { Priority } from './Priority';
import { TimesheetTime } from './TimesheetTime';

export type ReminderParameters = {
    fromName: string;
    fromEmail: string;
    toEmails: string[] | string;
    formatSubject: (data: TimesheetTime) => string;
    imageQuery?: string;
    message?: string;
    lastMessage?: string;
    priority?: Priority;
};

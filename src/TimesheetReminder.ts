import { IEmailManager, IImageService, ITimeManager } from './Interfaces';
import { ReminderParameters } from './Models';

export default class TimesheetReminder {
    #timeManager: ITimeManager;
    #memeGenerator: IImageService;
    #emailManager: IEmailManager;
    #isDevEnvironment: boolean;

    constructor(timeManager: ITimeManager, imageService: IImageService, emailManager: IEmailManager, isDevEnvironment: boolean = false) {
        this.#timeManager = timeManager;
        this.#memeGenerator = imageService;
        this.#emailManager = emailManager;
        this.#isDevEnvironment = isDevEnvironment;
    }

    public async remind({ fromName, fromEmail, toEmails, formatSubject, imageQuery = 'timesheet memes', message = 'Gentle reminder to submit your timesheet of the month.', lastMessage = 'Gentle reminder to submit your timesheet of the previous month.', priority = 'high' }: ReminderParameters): Promise<void> {
        if (!this.#timeManager.isSubmissionDay()) {
            throw new Error('Check that the date is valid for the run!');
        }

        const timesheetTime = this.#timeManager.getComputedTimesheetTime();
        const emails = this.#isDevEnvironment ? toEmails : await this.#emailManager.getEmails(timesheetTime);
        const randomMeme = await this.#memeGenerator.getImage(imageQuery);
        const content = timesheetTime.isFinalTimesheetTime ? lastMessage : message;

        this.#emailManager.sendEmail({
            fromName,
            fromEmail,
            toEmails,
            bccEmails: emails,
            contentAsText: `Hi there 😊\n${content}\nHave a good day!\n\nThis email is automatic, do not reply.`,
            contentAsHtml: `<html>
                <body>
                    <p>Hi there 😊</p>
                    <p>${content}</p>
                    <p>Have a good day!</p>
                    <img src="${randomMeme}" alt="timesheet meme" />

                    <br/>
                    <p>This email is automatic, do not reply.</p>
                </body>
            </html>`,
            subject: formatSubject(timesheetTime),
            priority
        });
    }
}

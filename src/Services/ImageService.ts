import fetch from 'node-fetch';

import { IImageService } from '../Interfaces';

export class ImageService implements IImageService {
    #apiKey: string;
    #baseUrl: string;

    constructor(apiKey: string, baseUrl: string) {
        this.#apiKey = apiKey;
        this.#baseUrl = baseUrl;
    }

    #getRandomIndex(max: number): number {
        return Math.floor(Math.random() * max);
    }

    public async getImage(query: string): Promise<string> {
        const params = new URLSearchParams({
            q: query,
            tbm: 'isch',
            ijn: '0', // page number,
            api_key: this.#apiKey
        });
        const url = `${this.#baseUrl}?${params}`;
        const response = await fetch(url, {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' }
        });
        const data = await response.json();

        if ('error' in data) throw new Error(data.error);

        const index = this.#getRandomIndex(data.images_results.length);
        const randomMeme = data.images_results[index].original;

        return randomMeme;
    }
}

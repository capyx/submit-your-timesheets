import { ITimeManager } from '../Interfaces';
import { TimesheetTime } from '../Models';

export class TimeManager implements ITimeManager {
    #today: Date;
    #previousMonth: Date;
    #dayOfFinalTimesheetSubmission: number;
    #dayOfProvisionalTimesheetSubmission: number;

    constructor(dayOfProvisionalTimesheetSubmission: number, dayOfFinalTimesheetSubmission: number, todayDate: Date = new Date()) {
        this.#dayOfProvisionalTimesheetSubmission = dayOfProvisionalTimesheetSubmission;
        this.#dayOfFinalTimesheetSubmission = dayOfFinalTimesheetSubmission;

        this.#today = todayDate;
        this.#previousMonth = new Date(this.#today.getFullYear(), this.#today.getMonth() - 1, 1);
    }

    public getComputedTimesheetTime(): TimesheetTime {
        let date;
        const isFinalTimesheetTime = this.#today.getDate() === this.#dayOfFinalTimesheetSubmission;

        if (isFinalTimesheetTime)
            date = this.#previousMonth;
        else
            date = this.#today;

        const data = {
            month: parseInt(date.toLocaleString('default', { month: 'numeric' })),
            year: date.getFullYear(),
            monthAsString: date.toLocaleString('default', { month: 'long' }),
            isFinalTimesheetTime
        };

        return data;
    }

    public isSubmissionDay(): boolean {
        const isProvisionDay = this.#today.getDate() === this.#dayOfProvisionalTimesheetSubmission;
        const isFinalDay = this.#today.getDate() === this.#dayOfFinalTimesheetSubmission;
        const isSubmissionDay = isProvisionDay || isFinalDay;
        return isSubmissionDay;
    }
}

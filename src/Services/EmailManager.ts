import fetch from 'node-fetch';
import * as nodemailer from 'nodemailer';
import { Credentials } from 'nodemailer/lib/smtp-connection';

import { TimesheetTime, User, EmailData } from '../Models';
import { IEmailManager } from '../Interfaces';

const INCLUDE_PROVISIONAL_AND_FINAL_TIMESHEETS = 1;
const INCLUDE_PROVISIONAL_TIMESHEET_ONLY = 0;

export class EmailManager implements IEmailManager {
    #apiKey: string;
    #baseUrl: string;
    #senderCredentials: Credentials;

    constructor(apiKey: string, baseUrl: string, senderCredentials: Credentials) {
        this.#apiKey = apiKey;
        this.#baseUrl = baseUrl;
        this.#senderCredentials = senderCredentials;
    }

    public async getEmails(data: TimesheetTime): Promise<string[]> {
        const { month, year, isFinalTimesheetTime } = data;
        const response = await fetch(`${this.#baseUrl}/users/late_on_timesheet?month=${month}&year=${year}&type=${isFinalTimesheetTime
            ? INCLUDE_PROVISIONAL_AND_FINAL_TIMESHEETS
            : INCLUDE_PROVISIONAL_TIMESHEET_ONLY}`,
            {
                method: 'GET',
                headers: { ApiKey: this.#apiKey }
            });
        const users: User[] = response.status === 200 ? await response.json() as User[] : [];
        const emails = users.map((user: User) => user.email);
        return emails;
    }

    public async sendEmail({ fromEmail, fromName, toEmails, subject, contentAsHtml = '<p>Gentle reminder to submit your timesheet of the month.</p>', contentAsText = 'Gentle reminder to submit your timesheet of the month.', bccEmails = [], priority = 'high' }: EmailData): Promise<void> {
        const mailOptions = {
            sender: fromEmail,
            from: `${fromName} <${fromEmail}>`,
            to: toEmails,
            bcc: bccEmails,
            subject,
            text: contentAsText,
            html: contentAsHtml,
            priority
        };

        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: this.#senderCredentials,
            tls: {
                rejectUnauthorized: false
            }
        });
        await transporter.verify();
        await transporter.sendMail(mailOptions);
    }
}

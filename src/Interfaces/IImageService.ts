export interface IImageService {
    getImage(query: string): Promise<string>;
}

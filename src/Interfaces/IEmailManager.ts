import { EmailData, TimesheetTime } from '../Models';

export interface IEmailManager {
    getEmails(data: TimesheetTime): Promise<string[]>;
    sendEmail(emailData: EmailData): Promise<void>;
}

import { TimesheetTime } from '../Models';

export interface ITimeManager {
    getComputedTimesheetTime(): TimesheetTime;
    isSubmissionDay(): boolean;
}

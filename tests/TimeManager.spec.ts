import { TimeManager } from '../src/Services/TimeManager';

describe('TimeManager', () => {
    it('should instanciate properly', () => {
        expect(() => new TimeManager(0, 0)).not.toBeNull();
    });

    describe('computeTimesheetTime', () => {

        // getMonth returns values between 0 and 11
        // but the manager returns a month between 1 and 12

        it('should return today\'s data when no submission time is not met', () => {
            const today = new Date();
            const currentMonth = today.getMonth() + 1;
            const manager = new TimeManager(0, 0);
            const result = manager.getComputedTimesheetTime();
            expect(result).toMatchObject({ month: currentMonth });
        });

        it('should return today\'s data when the provisional submission time is met', () => {
            const today = new Date();
            const currentMonth = today.getMonth() + 1;
            const manager = new TimeManager(today.getDate(), 0);
            const result = manager.getComputedTimesheetTime();
            expect(result).toMatchObject({ month: currentMonth });
        });

        it.each([
            ['2022-01-31'],
            ['2022-06-31'],
            ['2022-12-31'],
            ['2022-01-01'],
            ['2022-06-01'],
            ['2022-12-01']
        ])('should return previous month\'s data when the final submission time is met on the %s', (date: string) => {
            const today = new Date(date);
            const previousMonth = today.getMonth() === 0 ? 12 : today.getMonth();
            const manager = new TimeManager(0, today.getDate(), today);
            const result = manager.getComputedTimesheetTime();
            expect(result).toMatchObject({ month: previousMonth });
        });
    });

    describe('isSubmissionDay', () => {
        it('should return true when today is the provisional day', () => {
            const today = new Date();
            const manager = new TimeManager(today.getDate(), 0);
            expect(manager.isSubmissionDay()).toBe(true);
        });

        it('should return true when today is the final day', () => {
            const today = new Date();
            const manager = new TimeManager(0, today.getDate());
            expect(manager.isSubmissionDay()).toBe(true);
        });

        it('should return false when today is neither the provisional nor the final day', () => {
            const manager = new TimeManager(0, 0);
            expect(manager.isSubmissionDay()).toBe(false);
        });
    });
});
// This file must be in plain JS
// due to declaration emit in tsconfig
// and private/protected properties in the generated mock
// see: https://github.com/microsoft/TypeScript/issues/30355

const nodemailer = require('nodemailer');
const nodemailermock = require('nodemailer-mock').getMockFor(nodemailer);
module.exports = nodemailermock;

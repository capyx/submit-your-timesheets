import fetchMock from 'jest-fetch-mock';
fetchMock.enableMocks();

import * as nodemailer from 'nodemailer';
import { NodemailerMock } from 'nodemailer-mock';
const { mock: nodemailerMock } = nodemailer as unknown as NodemailerMock;

import { TimesheetTime } from '../src/Models';
import { EmailManager } from '../src/Services/EmailManager';

describe('EmailManager', () => {
    it('should instanciate properly', () => {
        expect(() => new EmailManager(expect.any(String), expect.any(String), {
            user: expect.any(String),
            pass: expect.any(String)
        })).not.toBeNull();
    });

    describe('getEmails', () => {
        it('should return the emails from the third party provider', async () => {
            const expected = ['example@capyx.be'];
            const timesheetTime: TimesheetTime = {
                month: expect.any(Number),
                year: expect.any(Number),
                isFinalTimesheetTime: expect.any(Boolean),
                monthAsString: expect.any(String)
            };
            const manager = new EmailManager(expect.any(String), expect.any(String), {
                user: expect.any(String),
                pass: expect.any(String)
            });
            fetchMock.mockResponse(() => Promise.resolve(JSON.stringify([{ email: 'example@capyx.be' }])));

            const actual = await manager.getEmails(timesheetTime);

            expect(actual).toStrictEqual(expected);
        });

        it('should return an empty array if nothing is returned from the third party provider', async () => {
            const timesheetTime: TimesheetTime = {
                month: expect.any(Number),
                year: expect.any(Number),
                isFinalTimesheetTime: expect.any(Boolean),
                monthAsString: expect.any(String)
            };
            const manager = new EmailManager(expect.any(String), expect.any(String), {
                user: expect.any(String),
                pass: expect.any(String)
            });
            fetchMock.mockResponse(() => Promise.resolve(JSON.stringify([])));

            const actual = await manager.getEmails(timesheetTime);

            expect(actual).toHaveLength(0);
        });
    });

    describe('sendEmail', () => {
        afterEach(() => {
            nodemailerMock.reset();
        });

        it('should send the email to the list of provided emails', async () => {
            const toEmails = ['example@capyx.be'];
            const manager = new EmailManager(expect.any(String), expect.any(String), {
                user: expect.any(String),
                pass: expect.any(String)
            });

            await manager.sendEmail({
                fromEmail: expect.any(String),
                toEmails: toEmails,
                fromName: expect.any(String),
                subject: expect.any(String),
                contentAsHtml: expect.any(String),
                contentAsText: expect.any(String)
            });

            const sentMails = nodemailerMock.getSentMail();
            expect(sentMails.length).toBe(1);
            expect(sentMails[0].to).toStrictEqual(toEmails);
        });
    });
});

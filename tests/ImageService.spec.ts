import fetchMock from 'jest-fetch-mock';
fetchMock.enableMocks();

import { ImageService } from '../src/Services/ImageService';

describe('ImageService', () => {
    it('should instanciate properly', () => {
        expect(() => new ImageService(expect.any(String), expect.any(String))).not.toBeNull();
    });

    describe('getImage', () => {
        it('should return the URL of an image randomly on Google Image given a query', async () => {
            const expectedImageUrl = 'https://img.google.com/test.png';
            const service = new ImageService(expect.any(String), expect.any(String));
            fetchMock.mockResponse(() => Promise.resolve(JSON.stringify({ images_results: [{ original: expectedImageUrl }] })));

            const actual = await service.getImage(expect.any(String));

            expect(actual).toBe(expectedImageUrl);
        });
    });
});

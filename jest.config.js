module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  transform: {
    '^.+\\.ts?$': 'ts-jest',
  },
  transformIgnorePatterns: ['<rootDir>/node_modules/'],
  modulePathIgnorePatterns: ['<rootDir>/build', '<rootDir>/node_modules/'],
  collectCoverageFrom: ['src/**/*.ts', '!**/node_modules/**'],
  coverageReporters: ['html', 'text', 'text-summary', 'cobertura'],
  reporters: ['default', [ 'jest-junit', { outputDirectory: 'coverage' }]],
  testMatch: ['**/tests/**/*.spec.ts'],
  coverageDirectory: '<rootDir>/coverage'
};